#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <queue>
#include <algorithm>


#define DATA_FILE "data.txt"
#define OUTPUT_FILE "output.txt"
#define SLEEP_TIME 60


class MessageSender
{
public:

	//constructor and destructor
	MessageSender();
	~MessageSender();

	//printers
	std::string printMenu() const;
	void printConnectedUsers() const;

	//user managment
	void signIn(const std::string user);
	void signOut(const std::string user);

	//file realted
	void readData();
	void sendMsgs();

private:

	//variables
	std::vector<std::string> _userList;
	std::queue<std::string> _msgList;

	std::mutex _fileMtx;
	std::mutex _usersMtx;
	std::condition_variable cond;

	//functions
	void clearData();

};

