#include "MessagesSender.h"


/*
The constructor of the MessageSender class
*/
MessageSender::MessageSender()
{
	//no elements to construct
}


/*
The destructor of the MessageSender class
it cleares the users list and the msgs queue
*/
MessageSender::~MessageSender()
{
	//creates an empty queue and swaps the full one with it
	std::queue<std::string> empty;
	std::swap(_msgList, empty);

	_userList.clear();  //clears the user vector
}


/*
The function prints the functions available to the user from MessageSender class
it prints the option to sign in a new user, to sign out a signed in user, to print all users, and destruct the class
Output: string which symblize the user's option from the menu
*/
std::string MessageSender::printMenu() const
{
	std::string option = "";

	std::cout << "Message Sender options:" << std::endl;
	std::cout << "1. Signin" << std::endl;
	std::cout << "2. Signout" << std::endl;
	std::cout << "3. Connected Users" << std::endl;
	std::cout << "4. Exit" << std::endl;
	std::cin >> option;

	return option;
}


/*
The function prints all the signed in users appeared in the class's vector
*/
void MessageSender::printConnectedUsers() const
{
	int i = 0;

	std::cout << "Signed in users list: " << std::endl;
	for (i = 0; i < _userList.size(); i++)  //a loop which runs on the signed in users and prints each one
	{
		std::cout << _userList[i] << std::endl;
	}
}


/*
The function checks if the given user name is already signed in, and if he doesnt, he will get added
Input: the user name to sign in
*/
void MessageSender::signIn(const std::string user)
{
	_usersMtx.lock();  //locks the user mutex before changing the shared resource _userList

	std::vector<std::string>::iterator pos = std::find(_userList.begin(), _userList.end(), user);  //gets the position of the given user name

	if (pos == _userList.end())  //if the user isnt the user's list 
	{
		_userList.push_back(user);  //signes in the user
	}
	else
	{
		std::cout << "The enetered user is already signed in..." << std::endl;
	}

	_usersMtx.unlock();  //unlocks the previously locked mutex
}


/*
The function checks if the given user name is already signed in, and if he is, he will get signed out
Input: the user name to sign out
*/
void MessageSender::signOut(const std::string user)
{
	_usersMtx.lock();  //locks the user mutex before changing the shared resource _userList

	std::vector<std::string>::iterator pos = std::find(_userList.begin(), _userList.end(), user);  //gets the position of the given user name

	if (pos != _userList.end())  //if the user is in the user's list
	{
		_userList.erase(pos);  //deletes the element in the posisiton of the givne user name
	}

	_usersMtx.unlock();  //unlocks the previously locked mutex
}


/*
The function reads the whole data.txt file and puts each line in it inside a vector of the class
*/
void MessageSender::readData()
{
	std::unique_lock<std::mutex> fileMtx(_fileMtx, std::defer_lock);
	std::string currLine;
	std::ifstream data;

	while (true)  //runs until the class activates it destrutor function
	{
		data.open(DATA_FILE);  //opens the file 

		fileMtx.lock();  //locks the file mutex before changing the shared resource _msgList

		while (getline(data, currLine))  //extracts each line of the data.txt file and puts inside the vector
		{
			_msgList.push(currLine);
		}

		fileMtx.unlock();  //unlocks the previously locked mutex

		data.close();
		clearData();
		
		cond.notify_one();  //notifies the send msgs thread that the msgs queue has new msgs
		std::this_thread::sleep_for(std::chrono::seconds(SLEEP_TIME));  //sleep for 60 seconds to allow changes in the data.txt file
	}
}


/*
The function updates the output.txt file by adding each line of the data.txt file in it for each signed in user from the class's vector and queue
*/
void MessageSender::sendMsgs()
{
	std::fstream output(OUTPUT_FILE, std::ios::app);  //opens the file writing at the end of it
	std::unique_lock<std::mutex> fileMtx(_fileMtx, std::defer_lock);
	std::unique_lock<std::mutex> usersMtx(_usersMtx, std::defer_lock);
	int i = 0;
	int j = 0;

	while (true)  //runs until the class activates it destrutor function
	{
		fileMtx.lock();  //locks the file mutex before changing the shared resource _msgList

		//wait for the read file thread to finish its exacution and will send msgs only if the list isnt empty
		cond.wait(fileMtx, [&]() { return !_msgList.empty(); });

		//the double loop runs on the user's vector and on the msgs queue to write the msgs to each user in the output.txt file
		for (i = 0; i < _msgList.size(); i++)  
		{
			usersMtx.lock();  //locks the user mutex before using the shared resource _userList

			for (j = 0; j < _userList.size(); j++)
			{
				output << _userList[j] << ": " << _msgList.front() << std::endl;  //writes to the output.txt file every first msg to each signed in user
			}

			usersMtx.unlock();  //unlocks the previously locked mutex

			_msgList.pop();  //clears the first msg out of the queue
		}

		fileMtx.unlock();  //unlocks the previously locked mutex
	}

	output.close(); 
}


/*
The funciton clears the content of the data.txt file
*/
void MessageSender::clearData()
{
	std::ofstream data(DATA_FILE, std::ios::out | std::ios::trunc);  //clears content of the data.txt file
	data.close();
}

