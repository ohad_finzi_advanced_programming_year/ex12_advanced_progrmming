#include "MessagesSender.h"


#define SIGNIN "1"
#define SIGNOUT "2"
#define CONNECTED_USERS "3"
#define EXIT "4"


int main()
{
	MessageSender msgSender;
	std::string option = "";
	std::string userName = "";

	//activates the readData and sendMsg functions from MessageSender class as threads
	std::thread callReadData(&MessageSender::readData, std::ref(msgSender));
	std::thread callSendMsgs(&MessageSender::sendMsgs, std::ref(msgSender));

	//deataches the created threads
	callReadData.detach();
	callSendMsgs.detach();

	while (option != EXIT)  //keeps running until the user enters the option 4
	{
		option = msgSender.printMenu(); 

		if (option == SIGNIN) 
		{
			std::cout << "Sign in user: ";
			std::cin >> userName;

			msgSender.signIn(userName);
		}
		else if (option == SIGNOUT)
		{
			std::cout << "Sign out user: ";
			std::cin >> userName;

			msgSender.signOut(userName);
		}
		else if (option == CONNECTED_USERS)
		{
			msgSender.printConnectedUsers();
		}
		else if (option == EXIT)
		{
			std::cout << "BYE BYE" << std::endl;
		}
		else
		{
			std::cout << "Invalid option, please try again: " << std::endl;
		}
	}

	return 0;
}

