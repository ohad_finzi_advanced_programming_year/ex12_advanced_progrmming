#include "threads.h"


/*
The function writes to a file all the primes numbers in the range of the two given numbers
Input: int begin and int end which symbolizes the range of numbers to search for primes and an open file to write the range of all the prime numbers in the range of the two given numbers
*/
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int i = 0;
	int j = 0;
	bool isPrime = true;

	for (i = begin; i <= end; i++)  //a loop which runs on the the given number range
	{
		isPrime = true;  //resets the check if prime element
		for (j = 2; j <= i / 2 && isPrime; j++)  //runs from the 2 to half of the current checkd number
		{
			if (i % j == 0)  //if the current checkd number can be divided by j, it means that is he is not a prime number
			{
				isPrime = false;  //breaks the loop
			}
		}

		if (isPrime)  //if the number current num is a prime number, it will get written to the given file
		{
			m.lock();  //locks all other threads before writing to the file

			file << i;
			file << '\n';

			m.unlock();  //unlocks after writing to the file to allow to other threads to proceed
		}	
	}
}


/*
The function creates a several threads (according to the given N integer)and each calls the function WritePrimesToFile
Input: int begin and int end which symbolizes the range of numbers to search for primes, int N which syblozies the number of threads to create, and the file path to put all the prime numbers in
*/
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file(filePath);

	std::thread* t = new std::thread [N];

	clock_t beginClock, endClock;
	double elapsed = 0;

	int range = (end - begin) / N;
	int newBegin = begin;
	int newEnd = begin;
	int i = 0;

	if (file.is_open())  //will create the threads only if the given path is valid
	{
		beginClock = clock();  //gets the current time (begin of all threads)

		for (i = 0; i < N; i++)
		{
			newEnd = newBegin + range;  //adds the range (calculated at the start) to the begin value
			t[i] = std::thread(writePrimesToFile, newBegin, newEnd, std::ref(file));  //creates a thread which activates the writePrimesToFile function with the correct elements
			newBegin = newEnd;  //sets the begin value to the end value for the next thread
		}

		for (i = 0; i < N; i++)
		{
			t[i].join();  //waits for the thread to end its run
		}

		endClock = clock();  //gets the current time (end of all threads)

		elapsed = double(endClock - beginClock) / CLOCKS_PER_SEC;  //calculates the time it took for all the threads to be over
		std::cout << "All threads exacution time: " << elapsed << std::endl;  //and prints it

		file.close();  //closes the file to prevent memory leakage
	}
	else  //incase the file isnt valid
	{
		std::cerr << "Unable to open file" << std::endl;
		exit(-1);
	}

	delete[] t;  //deletes the allocated dynamic memory
}

