#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <thread>
#include <mutex>
#include <time.h>


static std::mutex m;


void writePrimesToFile(int begin, int end, std::ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);

